group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.5.0"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("glib2") {
                includeDirs("/usr/include/glib-2.0")
            }
            dependencies {
                val guiVistaVer = "0.4.2"
                implementation("io.gitlab.gui-vista:guivista-core:$guiVistaVer")
            }
        }
        binaries {
            executable("obj_playground") {
                entryPoint = "org.example.objPlayground.main"
            }
        }
    }
}
