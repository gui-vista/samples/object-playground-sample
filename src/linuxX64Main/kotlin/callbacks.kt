package org.example.objPlayground

import glib2.g_ascii_strcasecmp
import glib2.gpointer

@Suppress("UNUSED_PARAMETER")
internal fun printStringElements(listData: gpointer?, userData: gpointer?) {
    if (listData != null) println("* Item: ${listData.stringValue}")
}

@Suppress("UNUSED_PARAMETER")
internal fun printIntElements(listData: gpointer?, userData: gpointer?) {
    if (listData != null) println("* Item: ${listData.intValue}")
}

@Suppress("UNUSED_PARAMETER")
internal fun findElement(item: gpointer?): Int = g_ascii_strcasecmp(item?.stringValue, "second")

internal fun compareStrings(valueA: gpointer?, valueB: gpointer?): Int =
    g_ascii_strcasecmp(valueA?.stringValue, valueB?.stringValue)
