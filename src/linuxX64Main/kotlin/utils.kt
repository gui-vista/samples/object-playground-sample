package org.example.objPlayground

import glib2.gpointer
import kotlinx.cinterop.*

internal val gpointer.stringValue: String
    get() = reinterpret<ByteVar>().toKString()
internal val gpointer.intValue: Int
    get() = reinterpret<IntVar>().pointed.value
