package org.example.objPlayground

import io.gitlab.guiVista.core.dataType.DoublyLinkedList
import io.gitlab.guiVista.core.dataType.SinglyLinkedList
import kotlinx.cinterop.*

internal fun doSinglyLinkedList() = memScoped {
    println("\n-- Singly Linked List --")
    val apple = "Apple"
    val pear = "Pear"
    val mango = "Mango"
    var list = SinglyLinkedList.create()

    println("This list is now ${list.length} items long")
    println("Prepending $apple...")
    list = list prepend apple.cstr.ptr
    println("Prepending $pear...")
    list = list prepend pear.cstr.ptr
    println("This list is now ${list.length} items long")
    println("Prepending $mango...")
    list = list prepend mango.cstr.ptr
    println("This list is now ${list.length} items long")
    list.iterateElements(staticCFunction(::printStringElements))
    list.close()
}

internal fun doDoublyLinkedList() = memScoped {
    println("\n-- Doubly Linked List --")
    val num1 = alloc<IntVar>().apply { value = 7 }
    val num2 = alloc<IntVar>().apply { value = 20 }
    val num3 = alloc<IntVar>().apply { value = 123 }

    var list = DoublyLinkedList.create()
    println("Appending ${num1.value}...")
    list = list append num1.ptr
    println("Appending ${num2.value}...")
    list = list append num2.ptr
    println("Appending ${num3.value}...")
    list = list append num3.ptr
    list.iterateElements(staticCFunction(::printIntElements))
    list.close()
}

internal fun doAddRemoveWithList() = memScoped {
    println("\n-- Add/Remove With Linked List --")
    val firstElement = "first".cstr.ptr
    var list = SinglyLinkedList.create()
    list = list append "second".cstr.ptr
    list = list prepend firstElement
    println("This list is now ${list.length} items long")
    list = list remove firstElement
    println("This list is now ${list.length} items long")
    list.close()
}

internal fun doRemoveDuplicatesWithList() = memScoped {
    println("\n-- Remove Duplicates With Linked List --")
    val secondElement = "second".cstr.ptr
    val thirdElement = "third".cstr.ptr
    var list = SinglyLinkedList.create()
    list = list append "first".cstr.ptr
    list = list append secondElement
    list = list append secondElement
    list = list append thirdElement
    list = list append thirdElement
    println("This list is now ${list.length} items long")
    list = list remove secondElement
    list = list removeAll thirdElement
    println("This list is now ${list.length} items long")
    list.close()
}

internal fun doDataPosWithList() = memScoped {
    println("\n-- Do Data Position With Linked List --")
    var list = SinglyLinkedList.create()
    list = list append "first".cstr.ptr
    list = list append "second".cstr.ptr
    list = list append "third".cstr.ptr
    println("The last item is: ${list.last?.data?.stringValue}")
    println("The item at index 1 is ${(list elementAt 1u)?.data?.stringValue}")
    println("Now the item at index 1 the easy way: ${(list dataAt 1u)?.stringValue}")
    println("And the next item after first item is: ${list.next?.data?.stringValue}")
}

private data class Person(val name: String, val age: UInt)

internal fun doCustomTypeWithList() = memScoped {
    println("\n-- Custom Type With Linked List --")
    val personRef1 = StableRef.create(Person("Tom Hanks", 15u))
    val personRef2 = StableRef.create(Person("Sarah Barrington", 65u))
    var list = SinglyLinkedList.create()
    list = list append personRef1.asCPointer()
    list = list append personRef2.asCPointer()
    println("Tom's Age: ${list.data?.asStableRef<Person>()?.get()?.age}")
    println("The last Person's name is: ${list.last?.data?.asStableRef<Person>()?.get()?.name}")
    list.close()
    personRef1.dispose()
    personRef2.dispose()
}

internal fun doConcatAndReverseWithList() = memScoped {
    println("\n-- Concat And Reverse With Linked List --")
    var list1 = SinglyLinkedList.create()
    var list2 = SinglyLinkedList.create()
    list1 = list1 append "first".cstr.ptr
    list1 = list1 append "second".cstr.ptr
    list2 = list2 append "third".cstr.ptr
    list2 = list2 append "fourth".cstr.ptr
    val both = list1 concat list2
    println("The third item in the concatenated list is ${(both dataAt 2u)?.stringValue}")
    val reversed = both.reverse()
    println("The first item in the reversed list is ${reversed.data?.stringValue}")
    reversed.close()
}

internal fun doListSorting() = memScoped {
    println("\n-- Linked List Sorting --")
    var list = SinglyLinkedList.create()
    list = list append "Chicago".cstr.ptr
    list = list append "Boston".cstr.ptr
    list = list append "Albany".cstr.ptr
    list = list.sort(staticCFunction(::compareStrings))
    println("The first item is now ${list.data?.stringValue}")
    println("The last item is now ${list.last?.data?.stringValue}")
    list.close()
}

internal fun doFindElementInList() = memScoped {
    println("\n-- Find Element In Linked List --")
    val second = "second".cstr.ptr
    var list = SinglyLinkedList.create()
    list = list append "first".cstr.ptr
    list = list append second
    list = list append "third".cstr.ptr
    var item = list.find(second)
    println("This should be the second item: ${item?.data?.stringValue}")
    item = list.findCustom(staticCFunction(::findElement).reinterpret())
    println("Again this should be the second item: ${item?.data?.stringValue}")
    item = list.find("delta".cstr.ptr)
    println("Note that 'delta' isn't in the list so we get: ${item?.data?.stringValue}")
    list.close()
}

internal fun doDoublyLinkedListNavigation() = memScoped {
    println("\n-- Doubly Linked List Navigation --")
    var list = DoublyLinkedList.create()
    list = list append "Austin".cstr.ptr
    list = list append "Bowie".cstr.ptr
    list = list append "Charleston".cstr.ptr
    println("Here is the list:")
    list.iterateElements(staticCFunction(::printStringElements))
    val last = list.last
    println("The first item: ${last?.first?.data?.stringValue}")
    println("The next to last item is ${last?.prev?.data?.stringValue}")
    println("The next to last item is ${last?.elementAtPrevious(1u)?.data?.stringValue}")
    list.close()
}

internal fun doRemoveLinksInList() = memScoped {
    println("\n-- Remove Links In Linked List --")
    var list = DoublyLinkedList.create()
    list = list append "Austin".cstr.ptr
    list = list append "Bowie".cstr.ptr
    list = list append "Chicago".cstr.ptr
    println("Here is the list:")
    list.iterateElements(staticCFunction(::printStringElements))
    val bowie = list elementAt 1u
    if (bowie != null) list = list removeLink bowie
    println("Here's the list after the removeLink call:")
    list.iterateElements(staticCFunction(::printStringElements))
    val tmp = list elementAt 1u
    if (tmp != null) list = list deleteLink tmp
    println("Here's the list after the deleteLink call:")
    list.iterateElements(staticCFunction(::printStringElements))
    list.close()
}
