package org.example.objPlayground

import glib2.G_TYPE_STRING
import io.gitlab.guiVista.core.Value
import io.gitlab.guiVista.core.dataType.variant.Variant
import io.gitlab.guiVista.core.dataType.variant.VariantDictionary
import io.gitlab.guiVista.core.dateTime.DateTime

fun main() {
    println("Starting Object Playground...")
    doVariant()
    doValue()
    doDateTime()
    runLinkedListExercises()
}

private fun runLinkedListExercises() {
    doSinglyLinkedList()
    doDoublyLinkedList()
    doAddRemoveWithList()
    doRemoveDuplicatesWithList()
    doDataPosWithList()
    doCustomTypeWithList()

    doConcatAndReverseWithList()
    doListSorting()
    doFindElementInList()
    doDoublyLinkedListNavigation()
    doRemoveLinksInList()
}

private fun doVariant() {
    println("\n-- Variant --")
    val strVariant = Variant.fromString("Hello World! :)")
    println("String Variant Value: ${strVariant.stringValue}")
    strVariant.close()
    val dict = VariantDictionary.create()
    dict.insertEntry("answerToUniverse", Variant.fromUInt(42u))
    println("Value From Dictionary: ${dict.lookupValue(key = "answerToUniverse")?.uIntValue}")
    dict.close()
}

private fun doValue() {
    println("\n-- Value --")
    val value = Value.create(G_TYPE_STRING)
    value.changeStaticString("Hello World! :)")
    println("Value: ${value.fetchString()}")
    value.close()
}

private fun doDateTime() {
    println("\n-- Date/Time --")
    val dt = DateTime.nowWithLocalTimeZone()
    if (dt != null) {
        val month = if (dt.month < 10) "0${dt.month}" else "${dt.month}"
        val day = if (dt.dayOfMonth < 10) "0${dt.dayOfMonth}" else "${dt.dayOfMonth}"
        val minute = if (dt.minute < 10) "0${dt.minute}" else "${dt.minute}"
        val second = if (dt.second < 10) "0${dt.second}" else "${dt.second}"

        println("Current ${dt.timeZoneAbbrev} Date: ${dt.year}-$month-$day")
        println("Current ${dt.timeZoneAbbrev} Time: ${dt.hour}:$minute:$second")
    }
}
